<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projetos_crisecia_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't$?&`-6D)q{NL<i7|m%=tJr)YLm3#FK^_MYspeb+mV1Ll9.OsftYfo:j0z.Is`)B');
define('SECURE_AUTH_KEY',  '4$33h;ykZ:K^SGJ7LI565v/MbVjZW?b)L<[oStq%4N9Dt+c XVP)xqo~PI.QShx=');
define('LOGGED_IN_KEY',    ':LSQZL*%1M]hVTiXPp0OXt +Fi@m0yDW#{C86W-2hl&w}e)1~v@X8;V M0S;K:1H');
define('NONCE_KEY',        'D,daWXBjDAs2@=:&wiR426y|ZPbW6}&$U}Z7EFoE4p-;[U}Tz(IX00WJOp;u5/:h');
define('AUTH_SALT',        'P{q>r@(L/ JdnzOXX{cMLi<mLsuH2jk^N$<vVv]~tl HP)>tJ,t^ZI-iQB-0+8Bp');
define('SECURE_AUTH_SALT', 'B/I fR8(;TI|^|IRENV{97e?21Y=*$Kd~>yytaHG_1Qk!rVX&5([d6.DjlVXrp:H');
define('LOGGED_IN_SALT',   'zm=(tna;!6(cvb|SG 7xHII0 va~mv{Wfo:2,$abcs@`@qy=k,t*0H;zD2>@oqm9');
define('NONCE_SALT',       'f#7vjH%Vn4~**EFLddLSwr }kth>8wzxHAP^:=&1lUJ1cKz<u#q/q.7i1<<ss(d3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
